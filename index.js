const http = require("http");
http.createServer(function(request, response) {
	if(request.url == "/items" && request.method == "GET"){
		// GET Request
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data retrieved from database.')
	}
	// POST Request
	if(request.url == "/items" && request.method == "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be sent to the database.');
	}
	// PUT request
	if(request.url == "/items" && request.method == "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be updated in the database.');
	}
	// DELETE request
	if(request.url == "/items" && request.method == "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Data to be deleted from the database.');
	}
}).listen(4000);
console.log('Server is running at localhost:4000')